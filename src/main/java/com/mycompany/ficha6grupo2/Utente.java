/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ficha6grupo2;

/**
 *
 * @author pcoelho
 */
public class Utente {
    
    
    private String nome;
    private char sexo;
    private int idade;
    private double altura, peso, imc ;
    
    
    Utente(String n, char s, int i, double a, double p ){
        
        nome = n;
        sexo = s;
        idade = i;
        altura = a;
        peso = p;
         
    }
    Utente(){
    
    }


    // Alterar qualquer atributo do utente set
    public void setNome(String nome){
        this.nome = nome;      
    }
    
    public void setSexo(char sexo){
        this.sexo = sexo;      
    }
    
    public void setIdade(int idade){
        this.idade = idade;      
    }
    
    public void setAltura(double altura){
        this.altura = altura;
    }
    
    public void setPeso(double peso){
        this.peso = peso;
    }
    

    // Consular todos os atributos do utente get
    public String getNome(){
        return this.nome;      
    }
    
    public char getSexo(){
        return this.sexo;      
    }
    
    public int getIdade(){
        return this.idade;      
    }
    
    public double getAltura(){
        return this.altura;
    }
    
    public double getPeso(){
        return this.peso;
    }
    
   public static String getDados(Utente u){
        
       String res = "";
       
        res += "Nome do Utente: " + u.nome + " ";
        res += "Sexo: " + u.sexo + " ";
        res += "Idade: " + u.idade + " ";
        res += "Altura: " + u.altura + " ";
        res += "Peso: " + u.peso + " \n";
    
        return res;
    }   
    
    
    //Calcular IMC
    public double imc(Utente u){
        double imc_utente = (u.peso/(Math.pow(u.altura,2)));
         
        this.imc = imc_utente;
        
        return imc_utente;
        
        }
    
    // Grau de Obesidade
    
    public String classificacao(double imc, int min, int max){

        
        if (imc < min) {  
            return "Magro";
        }
        
        if (imc >= min && imc <= max){
            return "Saudável";
        }
        
        return "Obeso";
    
    }
    
    
    // Optei para colocar na classe principal
    // idade diff entre dois utentes
    // check if saudavel    
    // change ref valus IMC
    
    
    
    
}
