/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ficha6grupo2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author pcoelho
 */
public class programa {
    
    public static void main(String[] args) {
         List<Utente>  ls = new ArrayList<>();
        
        int escolha;
        
        int min = 18;
        int max = 25;
        
        Scanner sc = new Scanner(System.in);
        do {
            menu();
            escolha = sc.nextInt();
            switch (escolha) {
                case 1:
                    introduzirUtente(ls);
                    break;
                case 2:
                    consultaUtente(ls);
                    break;
                case 3:
                    alterarUtente(ls);
                    break;
                    
                case 4:
                    System.out.println(diffIdade(ls));
                    break;
                    
                case 5:
                    System.out.println("O utente é : " + checkSaudavel(ls, min, max) + "\n");
                    break;
                
                case 6:
                    System.out.println("total de utentes: " + totalUtente(ls) + "\n");
                    break;
                
                case 7:
                    int limites[] = new int[2];
                    limites = updateClassificacao();
                    min = limites[0];
                    max = limites[1]; 
                    break;
                
                case 0:
                    escolha = 0;
                    break;
                
                
                default:
                    System.out.println("Valor escolhido inválido");
                    break;
            }
            
            
        } while (escolha != 0);
        
        
        
    }
 
    static void menu(){
        System.out.println("Opção 1: Introduzir utente:");
        System.out.println("Opção 2: Consultar utente:");
        System.out.println("Opção 3: Alterar atributos do utente:");
        System.out.println("Opção 4: Diferença de idades entre dois utentes:");
        System.out.println("Opção 5: Verificar se utente é saudável:");
        System.out.println("Opção 6: Obter números de utentes:");
        System.out.println("Opção 7: Alterar valores de referência para cálculo da obesidade:");
        System.out.println("Opção 0: Para sair:");
                
    }
    
    static void introduzirUtente(List ls){
     
        Scanner input = new Scanner(System.in);
        char escolha = 'n';
        do {
           
            System.out.println("Quer Introduzir um novo utente (s ou n)");
            escolha = input.next().charAt(0);
            if (escolha == 's') {
                
            System.out.println("Introduza Nome do utente : ");
            String nome = input.next();
            System.out.println("Introduza Genero do utente : ");
            char sexo = input.next().charAt(0);
            System.out.println("Introduza idade do utente : ");
            int idade = input.nextInt();
            System.out.println("Introduza altura do utente: ");
            double altura = input.nextDouble();
            System.out.println("Introduza peso do utente: ");
            double peso = input.nextDouble();
            
            
            Utente  u  = new Utente(nome,sexo,idade,altura,peso);
            ls.add(u);
            }
       
        } while (escolha !='n');
    }
    static void consultaUtente(List ls){
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Introduza o numero de Utente:");
        
        int nutente = sc.nextInt();
        
        if (nutente < ls.size() && nutente >=0) {
            Utente utente_ver = (Utente) ls.get(nutente);
            System.out.println(Utente.getDados(utente_ver));
        
        }
        else {
            System.out.println("Número de utente não existe\n");
        
        }

    }
    
    static void alterarUtente(List ls){
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Introduza o numero de Utente:");
        
        int nutente = sc.nextInt();
        
         if (nutente < ls.size()&& nutente >=0) {
            Utente user = new Utente();
            user =  (Utente) ls.get(nutente);
             
           System.out.println("O nome actual é " +  user.getNome() + " qual o novo nome  : ");
           String nome = sc.next();
           user.setNome(nome);
           System.out.println("O sexo actual é " +  user.getSexo() + " qual o novo sexo  : ");
           char sexo = sc.next().charAt(0);
           user.setSexo(sexo);
           System.out.println("O idade actual é " +  user.getIdade() + " qual a nova idade  : ");
           int idade = sc.nextInt();
           user.setIdade(idade);
           System.out.println("A altura actual é " +  user.getAltura() + " qual a nova altura  : ");
           double altura = sc.nextDouble();
           user.setAltura(altura);
           System.out.println("A peso actual é " +  user.getPeso() + " qual o novo peso  : ");
           double peso  = sc.nextDouble();
           user.setPeso(peso);
             
        
        }
        else {
            System.out.println("Número de utente não existe\n");
        
        }
    
    
    }
    
    static int diffIdade(List ls){
        int n1,n2;
        int diff = 0;
        Scanner sc = new Scanner(System.in);
        
        if (ls.size() >= 2){
        
        System.out.println("Escolha número do primeiro utente que quer comparar a idade:");
        n1 = sc.nextInt();
        
        System.out.println("Escolha número do segundo utente que quer comparar a idade:");
        n2 = sc.nextInt();
        
        Utente user1 = new Utente();
        Utente user2 = new Utente();
        
        user1 = (Utente) ls.get(n1);
        user2 = (Utente) ls.get(n2);
        
        diff = Math.abs(user1.getIdade() - user2.getIdade());
        
        }
        return diff;
    }
    
    static String checkSaudavel(List ls, int min, int max){
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Introduza o número de utente:");
        int nutente = sc.nextInt();
        
        Utente u = new Utente();
        u = (Utente) ls.get(nutente);
       double imc = u.imc(u);
       
       return u.classificacao(imc,min,max);       
    }
    
    static int totalUtente(List ls){
       
    return ls.size();
        
    }
    
    static int[] updateClassificacao(){
        int mini, maxi;
        int limites[] = new int[2];
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Qual o novo limite inferior:");
        limites[0] = sc.nextInt();
        System.out.println("Qual o novo limite superior:");
        limites[1] = sc.nextInt();
        
        return limites;
        
        
    
    }
    
    
            
    
}
